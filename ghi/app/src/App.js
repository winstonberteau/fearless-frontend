import {BrowserRouter, Route, Routes} from "react-router-dom"
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm'
import AttendConferenceForm from "./AttendConferenceForm";
import PresentationForm from "./PresentationForm";
import MainPage from "./MainPage";

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <Routes>
        <Route path="/">
          <Route index element={<MainPage />}></Route>
        </Route>
        <Route path="presentations">
          <Route path="new" element={<PresentationForm />}></Route>
        </Route>
        <Route path="attendees">
          <Route path="new" element={<AttendConferenceForm />}></Route>
        </Route>
        <Route path="conferences">
          <Route path="new" element={<ConferenceForm />}></Route>
        </Route>
        <Route path="attendees">
          <Route index element={<AttendeesList attendees={props.attendees} />}></Route>
        </Route>
        <Route path="locations">
          <Route path="new" element={<LocationForm />}></Route>
        </Route>
      </Routes>
      <div className="container">
        {/* <LocationForm /> */}
        {/* <AttendeesList attendees={props.attendees} /> */}
      </div>
    </BrowserRouter>
  );
}

export default App;
