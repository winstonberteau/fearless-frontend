import React, { useEffect, useState } from 'react';


function ConferenceForm() {
    const [locations, setLocations] = useState([]);
    const [formData, setFormData] = useState({
        name: '',
        starts: '',
        ends: '',
        description: '',
        max_presentations: '',
        max_attendees: '',
        location: '',
    })


    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({

          ...formData,


          [inputName]: value
        });
      }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                name: '',
                starts: '',
                ends: '',
                description: '',
                max_presentations: '',
                max_attendees: '',
                location: '',
              });
        };


    }


    return (
        <div className="row">
            <div className="offset-3 col-6"></div>
                <div className="shadow p-4 mt-4">
                <h1>Create a new conference</h1>
                <form onSubmit={handleSubmit} id="create-conference-form">
                <div className="form-floating mb-3">
                    <input onChange={handleFormChange} value={formData.name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                    <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleFormChange} value={formData.starts} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control"/>
                    <label htmlFor="starts">Starts</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleFormChange} value={formData.ends} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control"/>
                    <label htmlFor="ends">Ends</label>
                </div>
                <div className="form-floating mb-3">
                    <textarea onChange={handleFormChange} value={formData.description} placeholder="Description" required type="text" name="description" id="description" className="form-control"/>
                    <label htmlFor="description">Description</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleFormChange} value={formData.max_presentations} placeholder="Max Presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control"/>
                    <label htmlFor="Max Presentations">Maximum Presentation</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleFormChange} value={formData.max_attendees} placeholder="Max Attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"/>
                    <label htmlFor="Max Attendees">Maximum Attendees</label>
                </div>
                <div className="mb-3">
                <select onChange={handleFormChange} value={formData.location} required name="location" id="location" className="form-select">
                    <option value="">Choose a location</option>
                    {locations.map(location => {
                        return (
                            <option key={location.id} value={location.id}>
                                {location.name}
                            </option>
                        );
                    })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
        </div>
      </div>
    );
}



export default ConferenceForm;
