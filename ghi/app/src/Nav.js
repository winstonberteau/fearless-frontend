import { NavLink } from "react-router-dom";



function Nav() {
    return (
        <ul className="nav">
            <NavLink className="navbar-brand" to="/">Conference GO!</NavLink>
  <li className="nav-item">
    <NavLink className="nav-link" aria-current="page" to="/">Home</NavLink>
  </li>
  <li className="nav-item">
    <NavLink className="nav-link" to="locations/new">New Location</NavLink>
  </li>
  <li className="nav-item">
    <NavLink className="nav-link" to="conferences/new">New Conference</NavLink>
  </li>
  <li className="nav-item">
    <NavLink className="nav-link" to="presentations/new">New Presentation</NavLink>
  </li>
</ul>
    );
  }

  export default Nav;
