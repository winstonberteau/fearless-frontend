import React, { useEffect, useState } from 'react';


function ConferenceForm(props) {
    const [states, setStates] = useState([]);
    const [starts, setStarts] = useState('');

    return (
        <form className="d-flex">
            <input className="form-control me-2" type="search" placeholder="Search conferences" aria-label="Search"/>
            <button className="btn btn-outline-success" type="submit">Search</button>
        </form>
    )
}


export default ConferenceForm;
