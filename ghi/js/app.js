function createCard(name, description, pictureUrl, startDate, endDate, location) {
    return `
    <div class="col">
      <div class="card shadow p-3 mb-5 bg-body-tertiary rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        <div class="card-footer text-body-secondary">${startDate}-${endDate}</div>
        </div>
      </div>
    </div>
      `;
}

function errorAlert(){
    return `<div class="alert alert-danger" role="alert">
    An error has occurred
  </div>`
}


  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        console.log("response is bad!");
        let error = document.querySelector('h2');
        error.innerHTML += errorAlert();


      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = Date(details.conference.starts);
            const endDate = Date(details.conference.ends);
            const location = details.conference.location.name
            const html = createCard(name, description, pictureUrl, startDate, endDate, location);
            const row = document.querySelector('.row');

            row.innerHTML += html;



          }
        }

      }
    } catch (e) {
        console.error(e);
        console.log("error!");
        let error = document.querySelector('h2');
        error.innerHTML += errorAlert();
    }

  });
